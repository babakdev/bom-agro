<?php

namespace App\Http\Controllers;

use App\Category;
use App\Material;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function manageCategory()
    {
        $categories = Category::where('parent_id', '=', 0)->get();
        $allCategories = Category::select('title','id')->get();
        return view('bom.categoryTreeview',compact('categories','allCategories'));
    }


    public function addCategory(Request $request)
    {
        $this->validate($request, [
        		'title' => 'required',
        		'type' => 'required',
        		'ch_code' => 'required',
        		'ir_code' => 'required',
        		'qty' => 'nullable | numeric',
        	]);
        $input = $request->all();
        $input['parent_id'] = empty($input['parent_id']) ? 0 : $input['parent_id'];
        $input['qty'] = empty($input['qty']) ? 0 : $input['qty'];


        if (request()->hasFile('image')) {
            $image_tmp = $request->file('image');
            if ($image_tmp->isValid()) {
                $input = Category::saveImage($image_tmp, $input);
            }
        }

        Category::create($input);
        return back()->with('success', 'آیتم جدید اضافه شد');
    }

    public function editCategory(Request $request) {
        $item = Category::findOrFail($request->id);
        $mats = Material::get();


        if($request->isMethod('patch')) {
            $this->validate($request, [
        		'title' => 'required',
        		'type' => 'required',
        		'ch_code' => 'required',
        		'ir_code' => 'required',
        		'qty' => 'nullable | numeric',
        	]);
        $input = $request->all();

        if (request()->hasFile('image')) {
            $image_tmp = $request->file('image');
            if ($image_tmp->isValid()) {
                $input = Category::saveImage($image_tmp, $input);
                $prePhoto = Category::where('id', $item->id)->value('image');
                if (file_exists($prePhoto)) {
                    unlink($prePhoto);
                }
            }
        }

        $item->update($input);
        return redirect('/category-tree-view/')->with('success', 'آیتم ویرایش شد');
        }

        return view('bom.editCategory', compact('item','mats'));
    }

    public function show($id){
        $category = Category::findOrFail($id);
        $allCategories = Category::where('parent_id', $category->parent_id)
        ->where('id', '!=', $category->id)
        ->select('title','id')->get();
        $mats = Material::get();
        return view('bom.show', compact('category', 'allCategories', 'mats'));
    }

    public function destroy(){
        $category = Category::findOrFail(request('id'));
        $category->delete();
        return back()->with('success', 'آیتم حذف شد');
    }
}
