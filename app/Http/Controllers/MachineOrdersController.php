<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class MachineOrdersController extends Controller
{
    public function newMachineOrders()
    {
        $orders = Category::join('order_categories', 'order_categories.category_id', 'categories.id')
            ->where('order_categories.status', 0)
            ->where('order_categories.is_machine', 1)
            ->select('categories.title', 'categories.ch_code', 'categories.ir_code', 'categories.type', 'categories.desc'
                , 'order_categories.order_number', 'order_categories.status', 'order_categories.id',
                'order_categories.qty as orderQty', 'order_categories.category_id', 'order_categories.created_at')
            ->orderBy('order_categories.created_at', 'DESC')
            // ->groupBy('order_categories.order_number')
            ->paginate(20);

        return view('bom.orders.machines.newOrders', compact('orders'));
    }

    public function machinesBuilt()
    {
        $orders = Category::join('order_categories', 'order_categories.category_id', 'categories.id')
            ->where('order_categories.status', 1)
            ->where('order_categories.is_machine', 1)
            ->select('categories.title', 'categories.ch_code', 'categories.ir_code', 'categories.type', 'categories.desc'
                , 'order_categories.order_number', 'order_categories.status', 'order_categories.id',
                'order_categories.qty as orderQty', 'order_categories.category_id', 'order_categories.created_at')
            ->orderBy('order_categories.created_at', 'DESC')
            // ->groupBy('order_categories.order_number')
            ->paginate(20);

        return view('bom.orders.machines.madeOrders', compact('orders'));
    }

    public function newMachineOrder()
    {
        $machine = Category::findOrFail(request('id'));
        $qty = request('qty');

        if (count($machine->childs)) {
           $this->reversedFunc($machine);
        }
        return view('bom.orders.machines.newOrders', compact('child'));
    }

    protected function reversedFunc($machine){
        foreach ($machine->childs->where('type', '==', 'تولیدی') as $child) {
                dump($child);
                if (count($child->childs)){
                    $this->reversedFunc($child);
                }
        }
    }
}
