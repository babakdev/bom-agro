<?php

namespace App\Http\Controllers;

use App\OrderCategory;
use App\Category;
use Illuminate\Http\Request;

class OrderCategoryController extends Controller
{
    public function index()
    {
        $orders = Category::join('order_categories', 'order_categories.category_id', 'categories.id')
            ->where('order_categories.status', 0)
            ->where('order_categories.is_machine', 0)
            ->select('categories.title', 'categories.ch_code', 'categories.ir_code', 'categories.type', 'categories.desc'
                , 'order_categories.order_number', 'order_categories.status', 'order_categories.id',
                'order_categories.qty as orderQty', 'order_categories.category_id', 'order_categories.created_at')
            ->orderBy('order_categories.created_at', 'DESC')
            // ->groupBy('order_categories.order_number')
            ->get();

        return view('bom.orders.items.newOrders', compact('orders'));
    }

    public function itemBuilt()
    {
        $orders = Category::join('order_categories', 'order_categories.category_id', 'categories.id')
            ->where('order_categories.status', 1)
            ->where('order_categories.is_machine', 0)
            ->select('categories.title', 'categories.ch_code', 'categories.ir_code', 'categories.type', 'categories.desc'
                , 'order_categories.order_number', 'order_categories.status', 'order_categories.id',
                'order_categories.qty as orderQty', 'order_categories.category_id', 'order_categories.created_at')
            ->orderBy('order_categories.created_at', 'DESC')
            // ->groupBy('order_categories.order_number')
            ->paginate(20);

        return view('bom.orders.items.madeOrders', compact('orders'));
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $input = $request->all();
        foreach ($input as $id => $item) {
            $orderNumber = rand(111, 99999);
            if ($id != '_token' && $item) {
                $qty = Category::where('id', $id)->value('qty');
                OrderCategory::create([
                    'category_id' => $id,
                    'qty' => $item * $qty,
                    'order_number' => $orderNumber,
                    'status' => 0,
                    'is_machine' => 0,
                ]);
            }
        }
        return back()->with('success', 'سفارش ثبت شد');
    }

    public function show(OrderCategory $orderCategory)
    {
        //
    }

    public function update(Request $request)
    {
        $item = OrderCategory::findOrFail($request->id);
        $item->update(['qty' => $request->qty]);
        return back()->with('success', 'تعداد سفارش از قطعه مورد نظر ویرایش شد');
    }

    public function destroy()
    {
        $orderCategory = OrderCategory::findOrFail(request('id'));
        $orderCategory->delete();
        return back()->with('success', 'قطعه حذف شد');
    }

    public function madeItem()
    {
        $item = OrderCategory::findOrFail(request('id'));
        if ($item->status == 1) {
            $item->status = 0;
            $item->update();
            return back()->with('success', 'قطعه مورد نظر در لیست ساخته نشده ها قرار گرفت');
        }

        $item->status = 1;
        $item->update();
        return back()->with('success', 'قطعه مورد نظر در لیست ساخته شده ها قرار گرفت');
    }
}
