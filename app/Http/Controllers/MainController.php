<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\OrderDetail;
use App\OrdersImport;
use App\OrdersDetailsImport;
use Maatwebsite\Excel\Facades\Excel;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function charts() {
        return view('charts.chart');
    }

    public function sellCharts()
    {
        $from = date('2020-03-20');
        $to = date('2020-06-21');
        if(request('date') == 1) {
            $from = date('2020-03-20');
            $to = date('2020-06-21');
        } elseif(request('date') == 2) {
            $from = date('2020-06-21');
            $to = date('2020-09-22');

        } elseif(request('date') == 3) {
            $from = date('2020-09-22');
            $to = date('2020-12-21');
        } elseif(request('date') == 4) {
            $from = date('2020-12-21');
            $to = date('2021-02-20');
        }

        $sellByProduct = DB::table('order_details')
        ->select(DB::raw('sum(qty) as cnt'), 'code')
        ->groupBy('code')->where('code', '!=', 'S-C')
        ->whereBetween('created_at', [$from, $to])->get();

        $proNames = [];
        $proSells = [];
        foreach ($sellByProduct as $item) {
            $proNames[] = $item->code;
            $proSells[] = $item->cnt;
        }

        return view('charts.sells', [
            'proNames' => collect($proNames),
            'proSells' => collect($proSells),
            'data' => $sellByProduct->sortByDesc('cnt')
            ]);
    }

    public function importOrders()
    {
        if (request()->isMethod('post')) {

            if (request()->hasFile('tables')) {

                $data = Request::file('tables');
                foreach ($data as $item) {
                    Excel::import(new OrdersImport, $item, null, \Maatwebsite\Excel\Excel::XLS);
                }
                return back()->with('success', 'All good!');
            }
        }
        return view('charts.import_orders');
    }

    public function importOrderDetails()
    {
        if (request()->isMethod('post')) {

            if (request()->hasFile('tables')) {

                $data = Request::file('tables');
                foreach ($data as $item) {
                    $collection = (new OrdersDetailsImport)->toArray($item);
                    foreach ($collection as $item) {
                        if (isset($item[1][11])) {
                            $dateVerta = explode('/', $item[1][11]);
                            if (count($dateVerta) > 1) {
                                $myDate = Verta::getGregorian($dateVerta[0], $dateVerta[1], $dateVerta[2]);
                                $date = "$myDate[0]-$myDate[1]-$myDate[2] 00:00:00";

                                $order = Order::where('serial', $item[0][11])->first();

                                for ($i = 10; $i < 28; $i++) {
                                    $product = Product::where('code', $item[$i][2])->first();
                                    if (isset($item[$i][3])) {
                                        OrderDetail::create([
                                            'order_id' => $order->id,
                                            'product_id' => $product ? $product->id : 1,
                                            'name' => $product ? $product->name : $item[$i][3],
                                            'code' => $product ? $product->code : $item[$i][2],
                                            'price' => $item[$i][6],
                                            'qty' => $item[$i][4],
                                            'created_at' => $date,
                                            'updated_at' => $date,
                                            ]);
                                        }
                                    }
                                }
                            }
                        }

                        // Excel::import(new OrdersDetailsImport, $item, null, \Maatwebsite\Excel\Excel::XLS);
                    }
                    return back()->with('success', 'All good!');
                }
            }
        return view('charts.import_order_details');
    }
}
