<?php

namespace App;

use App\Order;
use App\Product;
use App\OrderDetail;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Hekmatinasser\Verta\Facades\Verta;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithMappedCells;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

class OrdersDetailsImport implements WithCalculatedFormulas, ToModel
{
    use Importable;

    public function model(array $row)
    {
    }
}
