<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;

class Category extends Model
{
    protected $guarded = [];

    public function childs() {
        return $this->hasMany('App\Category','parent_id','id') ;
    }

    public function material()
    {
        return $this->belongsTo(Material::class, 'mat_id');
    }

    static function saveImage($image_tmp, array $input): array
{
    $extension = $image_tmp->getClientOriginalExtension();
    $filename = rand(111, 99999) . '.' . $extension;
    $image_path = 'images/' . $filename;
    Image::make($image_tmp)->resize(500, 500)->save($image_path);
    $input['image'] = $image_path;
    return $input;
}

}
