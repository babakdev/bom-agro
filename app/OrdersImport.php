<?php

namespace App;

use Hekmatinasser\Verta\Facades\Verta;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithMappedCells;

class OrdersImport implements WithMappedCells, ToModel, WithCalculatedFormulas
{
    protected $i = 11;

    public function mapping(): array
    {
        return [
            'serial' => 'L1',
            'date' => 'L2',
            'total_amount' => 'L29',

            'code' => "C$this->i",
            'qty' => "E$this->i",
            'price' => "G$this->i",
        ];
    }

    public function model(array $row)
    {
        $oldOrder = Order::where('serial', $row['serial'])->count();

        if ($oldOrder != 0) {
            return back()->with('flash_error', 'فاکتور تکراری');
        }
        $total = ceil($row['total_amount']);
        $dateVerta = explode('/', $row['date']);
        if (count($dateVerta) > 1) {
            $myDate = Verta::getGregorian($dateVerta[0], $dateVerta[1], $dateVerta[2]);
            $date = "$myDate[0]-$myDate[1]-$myDate[2] 00:00:00";

            return new Order([
                'serial' => (string)$row['serial'],
                'date' => str_replace('/', '-', $row['date']),
                'total_amount' => $total,
                'customer_id' => '0',
                'order_status' => 'قدیمی',
                'created_at' => $date,
                'updated_at' => $date,
            ]);
        }

        // $product = Product::where('code', $row['code'])->first();
        // if ($product) {
        //     $orderDetail = new OrderDetail([
        //         'order_id' => $order->id,
        //         'product_id' => $product->id,
        //         'name' => $product->name,
        //         'code' => $product->code ?? '',
        //         'price' => $row['price'],
        //         'qty' => $row['qty'],
        //     ]);
        // }
        // return $order;
    }
}
