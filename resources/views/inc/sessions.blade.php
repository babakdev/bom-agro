<div class="col-md-12 text-center" dir="rtl">
    @if($message = Session::get('success'))
    <div class="alert alert-success alert-block text-center">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    @endif
</div>
@if(count($errors) > 0)
    <div class="text-center" dir="rtl">
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <ul class="list-style-none">
                @foreach($errors->all() as $error)
                    <li class="list-unstyled">{{$error}}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
