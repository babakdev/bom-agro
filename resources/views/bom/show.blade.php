@extends('layouts.adminLayout.admin_design')
@section('content')

<div class="container mb-5">
    <div class="breadcrumb text-info col-sm-12 p-1 m-0" dir="rtl">
        <div class="container-fluid text-right" style="display: contents">
            <a href="{{url('/')}}">داشبورد </a><i class="fa fa-chevron-left breadcrumb-item"></i>
            <a href="{{url('/category-tree-view')}}">  مدیریت BOM </a><i class="fa fa-chevron-left breadcrumb-item"></i>
            <a>{{ $category->title }} </a>
        </div>
    </div> <!--END BREADCRUMB-->
    @include('inc.sessions')

    <hr>
    <ul id="tree1" class="container">
        <div class="row mx-auto text-right" dir="rtl">
            <img src="{{  asset($category->image) }}" class="rounded img-fluid col-md-4">
            <div class=" col-md-8">
                <h3 class="text-center"> {{ $category->title }}</h3>
                <span><b>کد چین: </b><span style="font-family: sans-serif;font-size:small">{{ $category->ch_code }}</span></span><br>
                <span><b>کد اگرو: </b><span style="font-family: sans-serif;font-size:small">{{ $category->ir_code }}</span></span><br>
                <span><b>توضیحات: </b><span>{{ $category->desc }}</span></span>
            </div>
        </div>

        <div class="card text-right border" dir="rtl">
            <h4 class="card-header">سفارش این محصول</h4>
            <form action="/new-machine-order" method="POST" class="card-body">
                @csrf
                <input type="hidden" name="id" value="{{ $category->id }}">
                <div class="form-group {{ $errors->has('qty') ? 'has-error' : '' }} row">
                    <label for="qty" class="col-md-1 pt-2">تعداد:</label>
                    <input id="qty" type="number" class="form-control col-md-2" value="{{ old('qty') }}" name="qty" min="1" required>
                    <span class="text-danger">{{ $errors->first('qty') }}</span>
                    <button type="submit" class="btn btn-success btn-lg">ثبت</button>
                </div>
            </form>
        </div>


        <div class="row">
            <span class="bg-success px-2 mx-2"></span>&nbsp;<b>تولیدی</b>
            <span class="bg-danger px-2 mx-2"></span>&nbsp;<b>وارداتی</b>
            <span class="bg-primary px-2 mx-2"></span>&nbsp;<b>خرید از داخل کشور</b>
        </div>

        <hr>
        <!-- TABLE -->
        <button class="btn btn-secondary col" data-toggle="collapse" data-target="#coll"><small dir="ltr">(کلیک کنید)</small>&nbsp;<i class="fas fa-mouse-pointer"></i> جدول قطعات </button>
        <div id="coll" class="collapse">
            <button class="btn btn-danger mt-2" onclick="printContent('bomTable');">چاپ جدول</button>
            <div id="bomTable">
                <form action="/purchase-items" method="POST">
                    @csrf
            <table class="table table-responsive table-bordered text-center my-3" dir="rtl">
                <thead>
                    <h3 class="text-center"> {{ $category->title }}</h3>
                  <tr style="font-size: smaller">
                      <th style="width: 100px">تعداد سفارش</th>
                    <th>نام قطعه</th>
                    <th>تعداد</th>
                    <th>کد چین</th>
                    <th>کد ایران</th>
                    <th>نوع</th>
                    <th>متریال</th>
                    <th>توضیحات</th>
                  </tr>
                </thead>

                <tbody>

                    @foreach ($category->childs as $item)
                    <tr style="@if($item->qty == 0) background-color:rgb(222 222 222) !important; @endif">
                        <td>
                        @if($item->qty > 0)
                        <input type="number" class="form-control" name="{{ $item->id }}" value="{{ old($item->id) }}">
                            @else
                                <span>-----</span>
                        @endif
                    </td>
                        <td class="text-right">{{ $item->title }}</td>
                        <td>{{ $item->qty }}</td>
                        <td class="small" style="font-family: sans-serif;font-size:smaller">{{ $item->ch_code }}</td>
                        <td class="small" style="font-family: sans-serif;font-size:smaller">{{ $item->ir_code }}</td>
                        <td class="@if($item->type == 'وارداتی') text-danger @elseif($item->type == 'تولیدی') text-success @else text-primary @endif">{{ $item->qty == 0 ? '-' : $item->type }}</td>
                        <td></td>
                        <td class="small">{{ $item->desc }}</td>
                      </tr>

                      @if(count($item->childs))
                      @include('bom.tableRow',['childs' => $item->childs])
                      @endif
                    @endforeach

                </tbody>

              </table>
              <button id="add" type="submit" class="btn btn-success">ارسال سفارش</button>
            </form>
            </div>
        </div>

        <hr>

        <div class="border">
        <h3 class="bg-dark rounded text-white text-center py-2">BOM</h3>
        <div class="row mx-auto">
            <div class="col-md-10"></div>
            <button class="btn btn-danger my-3 col-md-2" onclick="printContent('bom');"> BOM چاپ</button>
        </div>
        <li id="bom" class="bg-white" style="list-style: none">
            <div class="row mx-auto p-2">
                <span dir="rtl"> {{$category->title }}</span>

                <button type="button" class="btn btn-link" data-toggle="modal" data-target="#newModal-{{ $category->id }}">
                    <small class="text-secondary" id="add">add</small>
                </button>
                    <!-- New Modal -->
                    <div class="modal fade text-right" id="newModal-{{ $category->id }}">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h4 class="modal-title">زیرمجموعه جدید</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <div class="modal-body" dir="rtl">
                                    <form action="/add-category" method="POST" enctype="multipart/form-data">
                                        @csrf

                                    <input type="hidden" name="parent_id" value="{{ $category->id }}">
                                        <div class="row">
                                            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }} col-md-6">
                                                <label>نام:</label>
                                                <input type="text" class="form-control" value="{{ old('title') }}" name="title" required>
                                                <span class="text-danger">{{ $errors->first('title') }}</span>
                                            </div>

                                            <div class="form-group {{ $errors->has('qty') ? 'has-error' : '' }} col-md-6">
                                                <label>تعداد:</label>
                                                <input type="number" class="form-control" value="{{ old('qty') }}" name="qty">
                                                <span class="text-danger">{{ $errors->first('qty') }}</span>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="form-group {{ $errors->has('ch_code') ? 'has-error' : '' }} col-md-6">
                                                <label>کد چین:</label>
                                                <input style="font-family: sans-serif;font-size:small" type="text" class="form-control text-left" dir="ltr" value="{{ old('ch_code') }}" name="ch_code" required>
                                                <span class="text-danger">{{ $errors->first('ch_code') }}</span>
                                            </div>

                                            <div class="form-group {{ $errors->has('ir_code') ? 'has-error' : '' }} col-md-6">
                                                <label>کد اگرو:</label>
                                                <input style="font-family: sans-serif;font-size:small" type="text" class="form-control text-left" dir="ltr" value="{{ old('ir_code') ?? $category->ir_code.'-' }}" name="ir_code" required>
                                                <span class="text-danger">{{ $errors->first('ir_code') }}</span>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }} col-md-6">
                                                <label>عکس:</label>
                                                <input type="file" class="" value="{{ old('image') }}" name="image">
                                                <span class="text-danger">{{ $errors->first('image') }}</span>
                                            </div>

                                            <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }} col-md-6">
                                                <label>نوع:</label>
                                                <select type="text" class="form-control" value="{{ old('type') }}" name="type">
                                                    <option value="تولیدی">تولیدی</option>
                                                    <option value="داخلی">داخلی</option>
                                                    <option value="وارداتی">وارداتی</option>
                                                </select>
                                                <span class="text-danger">{{ $errors->first('type') }}</span>
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('desc') ? 'has-error' : '' }} col-md-12">
                                            <label>توضیحات:</label>
                                            <input type="text" class="form-control" value="{{ old('desc') }}" name="desc">
                                            <span class="text-danger">{{ $errors->first('desc') }}</span>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success btn-block btn-lg">ثبت</button>
                                        </div>
                                    </form>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">کنسل</button>
                                </div>

                            </div>
                        </div>
                    </div>
            </div>
            <ul>
                @foreach($category->childs as $child)
                <li>
                    <div class="row">
                        @if(count($child->childs) > 0)
                    <button class="btn badge badge-pill" data-toggle="collapse" data-target="#coll-{{  $child->id }}"><i class="fa fa-plus"></i></button>
                    @endif
                        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal-{{ $child->id }}">
                            {{ $child->title }}
                        </button>

                        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#newModal-{{ $child->id }}">
                            <small class="text-secondary" id="add">add</small>
                        </button>
                    </div>

                    <!-- Cat Modal -->
                    <div class="modal fade text-right" id="myModal-{{ $child->id }}">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <div class="modal-body" dir="rtl">
                                    @if($child->image)
                                    <img src="{{ asset($child->image) }}" class="img-fluid rounded row mx-auto shadow mb-2" width="500">
                                    @endif
                                    <h4 class="text-center"> {{ $child->title }}</h4>
                                    <span><b>کد چین: </b><span style="font-family: sans-serif;font-size:small">{{ $child->ch_code }}</span></span><br>
                                    <span><b>کد اگرو: </b><span style="font-family: sans-serif;font-size:small">{{ $child->ir_code }}</span></span><br>
                                    <span><b>تعداد: </b><span>{{ $child->qty }}</span></span><br>
                                    <span><b>نوع: </b><span>{{ $child->type }}</span></span><br>
                                    <span><b>توضیحات: </b><span>{{ $child->desc ?? '-' }}</span></span>
                                </div>

                                <div class="modal-footer">
                                <a href="{{ url('/edit-category?id=' . $child->id) }}" class="btn btn-info text-white" >ویرایش</a>
                                <form action="{{ url('/remove-category') }}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <input type="hidden" name="id" value="{{$child->id}}">
                                    <button class="btn btn-danger" type="submit">حذف</button>
                                </form>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">کنسل</button>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!-- New Modal -->
                    <div class="modal fade text-right" id="newModal-{{ $child->id }}">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h4 class="modal-title">زیرمجموعه جدید</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <div class="modal-body" dir="rtl">
                                    <form action="/add-category" method="POST" enctype="multipart/form-data">
                                        @csrf

                                        <input type="hidden" name="parent_id" value="{{ $child->id }}">
                                        <div class="row">
                                            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }} col-md-6">
                                                <label>نام:</label>
                                                <input type="text" class="form-control" value="{{ old('title') }}" name="title">
                                                <span class="text-danger">{{ $errors->first('title') }}</span>
                                            </div>

                                            <div class="form-group {{ $errors->has('qty') ? 'has-error' : '' }} col-md-6">
                                                <label>تعداد:</label>
                                                <input type="number" class="form-control" value="{{ old('qty') }}" name="qty">
                                                <span class="text-danger">{{ $errors->first('qty') }}</span>
                                            </div>

                                        </div>


                                        <div class="row">
                                            <div class="form-group {{ $errors->has('ch_code') ? 'has-error' : '' }} col-md-12">
                                                <label>کد چین:</label>
                                                <input style="font-family: sans-serif;font-size:small" type="text" class="form-control text-left" dir="ltr" value="{{ old('ch_code') }}" name="ch_code">
                                                <span class="text-danger">{{ $errors->first('ch_code') }}</span>
                                            </div>

                                            <div class="form-group {{ $errors->has('ir_code') ? 'has-error' : '' }} col-md-12">
                                                <label>کد اگرو:</label>
                                                <input style="font-family: sans-serif;font-size:small" type="text" class="form-control text-left" dir="ltr" value="{{ old('ir_code') ?? $child->ir_code.'-' }}" name="ir_code">
                                                <span class="text-danger">{{ $errors->first('ir_code') }}</span>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }} col-md-6">
                                                <label>عکس:</label>
                                                <input type="file" class="" value="{{ old('image') }}" name="image">
                                                <span class="text-danger">{{ $errors->first('image') }}</span>
                                            </div>

                                            <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }} col-md-6">
                                                <label>نوع:</label>
                                                <select type="text" class="form-control" value="{{ old('type') }}" name="type">
                                                    <option value="تولیدی">تولیدی</option>
                                                    <option value="داخلی">داخلی</option>
                                                    <option value="وارداتی">وارداتی</option>
                                                </select>
                                                <span class="text-danger">{{ $errors->first('type') }}</span>
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('desc') ? 'has-error' : '' }} col-md-12">
                                            <label>توضیحات:</label>
                                            <input type="text" class="form-control" value="{{ old('desc') }}" name="desc">
                                            <span class="text-danger">{{ $errors->first('desc') }}</span>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success btn-block btn-lg">ثبت</button>
                                        </div>
                                    </form>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">کنسل</button>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div id="coll-{{  $child->id }}" class="collapse">
                        @if(count($child->childs))
                    @include('bom.manageChild',['childs' => $child->childs])
                    @endif
                        </div>

                </li>
                @endforeach
            </ul>
        </li>
    </ul>
</div>
</div>
@endsection
