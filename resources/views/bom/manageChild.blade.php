<ul class="shadow-sm mb-2" style="border-bottom: 1px solid #eee!important;border-left: 1px solid #eee!important">
    @foreach($childs as $child)
        <li>
            @if(count($child->childs) > 0)
            <button class="btn badge badge-pill" data-toggle="collapse" data-target="#coll-{{ $child->id }}"><i class="fa fa-plus"></i></button>
            @endif
            <button type="button" class="btn btn-link @if($child->type == 'وارداتی') text-danger @elseif($child->type == 'تولیدی') text-success @endif" data-toggle="modal" data-target="#myModal-{{ $child->id }}">
                <span dir="rtl">{{ $child->title }}</span>
            </button>
            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#newModal-{{ $child->id }}">
                <small class="text-secondary" id="add">add</small>
            </button>

             <!-- Cat Modal -->
             <div class="modal fade text-right" id="myModal-{{ $child->id }}">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="modal-body" dir="rtl">
                            @if($child->image)
                                    <img src="{{ asset($child->image) }}" class="img-fluid rounded row mx-auto shadow mb-2" width="500">
                                    @endif
                            <h4 class="text-center"> {{ $child->title }}</h4>
                            <span><b>کد چین: </b><span style="font-family: sans-serif;font-size:small">{{ $child->ch_code }}</span></span><br>
                            <span><b>کد اگرو: </b><span style="font-family: sans-serif;font-size:small">{{ $child->ir_code }}</span></span><br>
                            <span><b>تعداد: </b><span>{{ $child->qty }}</span></span><br>
                            <span><b>نوع: </b><span>{{ $child->type }}</span></span><br>
                            <span><b>توضیحات: </b><span>{{ $child->desc ?? '-' }}</span></span>
                        </div>

                        <div class="modal-footer">
                        <a href="{{ url('/edit-category?id=' . $child->id) }}" class="btn btn-info text-white" >ویرایش</a>
                         <form action="{{ url('/remove-category') }}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <input type="hidden" name="id" value="{{$child->id}}">
                                    <button class="btn btn-danger" type="submit">حذف</button>
                                </form>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">کنسل</button>
                        </div>

                    </div>
                </div>
            </div>

            <!-- New Modal -->
            <div class="modal fade text-right" id="newModal-{{ $child->id }}">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h4 class="modal-title">زیرمجموعه جدید</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="modal-body" dir="rtl">
                            <form action="/add-category" method="POST" enctype="multipart/form-data">
                                @csrf

                                <input type="hidden" name="parent_id" value="{{ $child->id }}">

                                <div class="row">
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }} col-md-6">
                                        <label>نام:</label>
                                        <input type="text" class="form-control" value="{{ old('title') }}" name="title">
                                        <span class="text-danger">{{ $errors->first('title') }}</span>
                                    </div>

                                    <div class="form-group {{ $errors->has('qty') ? 'has-error' : '' }} col-md-6">
                                        <label>تعداد:</label>
                                        <input type="number" class="form-control" value="{{ old('qty') }}" name="qty" >
                                        <span class="text-danger">{{ $errors->first('qty') }}</span>
                                    </div>

                                </div>


                                <div class="row">
                                    <div class="form-group {{ $errors->has('ch_code') ? 'has-error' : '' }} col-md-12">
                                        <label>کد چین:</label>
                                        <input style="font-family: sans-serif;font-size:small" type="text" class="form-control text-left" dir="ltr" value="{{ old('ch_code') }}" name="ch_code" required>
                                        <span class="text-danger">{{ $errors->first('ch_code') }}</span>
                                    </div>

                                    <div class="form-group {{ $errors->has('ir_code') ? 'has-error' : '' }} col-md-12">
                                        <label>کد اگرو:</label>
                                        <input style="font-family: sans-serif;font-size:small" type="text" class="form-control text-left" dir="ltr" value="{{ old('ir_code') ?? $child->ir_code.'-' }}" name="ir_code" required>
                                        <span class="text-danger">{{ $errors->first('ir_code') }}</span>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }} col-md-6">
                                        <label>عکس:</label>
                                        <input type="file" class="" value="{{ old('image') }}" name="image">
                                        <span class="text-danger">{{ $errors->first('image') }}</span>
                                    </div>

                                    <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }} col-md-6">
                                        <label>نوع:</label>
                                        <select type="text" class="form-control" value="{{ old('type') }}" name="type">
                                            <option value="تولیدی">تولیدی</option>
                                            <option value="داخلی">داخلی</option>
                                            <option value="وارداتی">وارداتی</option>
                                        </select>
                                        <span class="text-danger">{{ $errors->first('type') }}</span>
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('mat_id') ? 'has-error' : '' }} col-md-12">
                                    <label>متریال:</label>
                                    <select type="text" class="form-control" value="{{ old('mat_id') }}" name="mat_id">
                                        <option value="{{ $child->material ? $child->mat_id : '' }}">{{ $child->material ? $child->material->name : 'نامشخص' }}</option>
                                    @foreach($mats as $mat)
                                            <option value="{{ $mat->id }}">{{ $mat->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger">{{ $errors->first('type') }}</span>
                                </div>

                                <div class="row">
                                    <div class="form-group {{ $errors->has('length') ? 'has-error' : '' }} col-md-6">
                                        <label>طول:</label>
                                        <input style="font-family: sans-serif;font-size:small" type="number" class="form-control text-left" dir="ltr" value="{{ old('length') ?? $child->length }}" name="length">
                                        <span class="text-danger">{{ $errors->first('length') }}</span>
                                    </div>
                                    <div class="form-group {{ $errors->has('area') ? 'has-error' : '' }} col-md-6">
                                        <label>مساحت:</label>
                                        <input style="font-family: sans-serif;font-size:small" type="number" class="form-control text-left" dir="ltr" value="{{ old('area') ?? $child->area }}" name="area">
                                        <span class="text-danger">{{ $errors->first('area') }}</span>
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('desc') ? 'has-error' : '' }} col-md-12">
                                    <label>توضیحات:</label>
                                    <input type="text" class="form-control" value="{{ old('desc') }}" name="desc">
                                    <span class="text-danger">{{ $errors->first('desc') }}</span>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-success btn-block btn-lg">ثبت</button>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">کنسل</button>
                        </div>

                    </div>
                </div>
            </div>

            <div id="coll-{{  $child->id }}" class="collapse">
            @if(count($child->childs))
                @include('bom.manageChild',['childs' => $child->childs])
            @endif
            </div>
        </li>
    @endforeach
</ul>
