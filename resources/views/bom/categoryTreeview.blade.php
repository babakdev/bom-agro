@extends('layouts.adminLayout.admin_design')
@section('content')

    <div class="container">
        <div class="card border">
            <div class="card-header text-center h4"> دستگاه جدید</div>
            @include('inc.sessions')

            <div class="card-body">

                <form action="/add-category" method="POST" enctype="multipart/form-data" class="text-right" dir="rtl">
                    @csrf

                    <div class="row">
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }} col-md-4">
                            <label>نام:</label>
                            <input type="text" class="form-control" value="{{ old('title') }}" name="title">
                            <span class="text-danger">{{ $errors->first('title') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('ch_code') ? 'has-error' : '' }} col-md-4">
                            <label>کد چین:</label>
                            <input type="text" class="form-control text-left" dir="ltr" value="{{ old('ch_code') }}"
                                   name="ch_code">
                            <span class="text-danger">{{ $errors->first('ch_code') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('ir_code') ? 'has-error' : '' }} col-md-4">
                            <label>کد اگرو:</label>
                            <input type="text" class="form-control text-left" dir="ltr" value="{{ old('ir_code') }}"
                                   name="ir_code">
                            <span class="text-danger">{{ $errors->first('ir_code') }}</span>
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group {{ $errors->has('qty') ? 'has-error' : '' }} col-md-4">
                            <label>تعداد:</label>
                            <input type="number" class="form-control" value="{{ old('qty') }}" name="qty">
                            <span class="text-danger">{{ $errors->first('qty') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('desc') ? 'has-error' : '' }} col-md-4">
                            <label>توضیحات:</label>
                            <input type="text" class="form-control" value="{{ old('desc') }}" name="desc">
                            <span class="text-danger">{{ $errors->first('desc') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }} col-md-4">
                            <label>نوع:</label>
                            <select type="text" class="form-control" value="{{ old('type') }}" name="type">
                                <option value="تولیدی">تولیدی</option>
                                <option value="داخلی">داخلی</option>
                                <option value="وارداتی">وارداتی</option>
                            </select>
                            <span class="text-danger">{{ $errors->first('type') }}</span>
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }} col-md-4">
                        <label>عکس:</label>
                        <input type="file" class="" value="{{ old('image') }}" name="image">
                        <span class="text-danger">{{ $errors->first('image') }}</span>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-lg">ثبت</button>
                    </div>
                </form>


            </div>
        </div>

        <div class="col-md-12 border p-2 card shadow ">
            <h3 class="text-center card-header"> لیست دستگاه ها </h3>
            <div class="card-body">
                <ul id="tree1" class="row">
                    @foreach($categories as $category)
                        <li style="list-style: none" class="mb-2 card col-md-3 shadow">
                            <img src="{{ asset($category->image) ?? url('/images/NoImageAvailable.webp') }}" width="235"
                                 class="img-fluid mb-1 rounded">
                            <a href="{{ url('/category/' . $category->id) }}" class="btn btn-primary">
                                {{ $category->title }}
                            </a>
                            <a href="{{ url('/edit-category?id=' . $category->id) }}" class="btn btn-link">
                                <i class="fa fa-edit text-info"></i></a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

@endsection
