@foreach ($childs->where('type', '==', 'تولیدی') as $child)
    {{--@foreach ($childs as $child)--}}
    <tr style="@if($child->qty == 0) background-color:rgb(222 222 222) !important; @endif">

        <td>
            @if($child->qty > 0)
                <input type="number" class="form-control" name="{{ $child->id }}" value="{{ old($child->id) }}">
            @else
                <span>-----</span>
            @endif

        </td>
        <td class="text-right">@if($loop->first)&nbsp; @else&nbsp;&nbsp; @endif <a
                href="{{ url('/edit-category?id=' . $child->id) }}">{{ $child->title }}</a></td>
        <td>{{ $child->qty }}</td>
        <td class="small" style="font-family: sans-serif;font-size:smaller">{{ $child->ch_code }}</td>
        <td class="small" style="font-family: sans-serif;font-size:smaller">{{ $child->ir_code }}</td>
        <td class="@if($child->type == 'وارداتی') text-danger @elseif($child->type == 'تولیدی') text-success @else text-primary @endif">{{ $child->type }}</td>
        <td class="small">{{ $child->material ? $child->material->name : '' }}</td>
        <td class="small">{{ $child->desc }}</td>
    </tr>
    @if(count($child->childs))
        @include('bom.tableRow',['childs' => $child->childs])
    @endif

@endforeach
