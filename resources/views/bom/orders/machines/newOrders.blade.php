@extends('layouts.adminLayout.admin_design')
@section('content')
<div class="container">
    @include('inc.sessions')
    @if(count($orders)>0)
    <button class="btn btn-danger mt-2" onclick="printContent('newOrders');">چاپ جدول</button>
    <div id="newOrders">
<h3 class="text-center">لیست سفارش ساخت قطعات</h3>
    <table id="example" class="table table-striped table-bordered table-hover text-center" style="overflow: auto"
    dir="rtl">
    <thead>
        <tr class="text-light bg-dark">
            <th class="px-0" style="font-size: 10px">شماره سفارش</th>
            <th>نام قطعه</th>
            <th>کد اگرو</th>
            <th>کد چین</th>
            <th class="px-0" style="font-size: 10px">تعداد</th>
            <th>نوع</th>
            <th>توضیحات</th>
            <th id="actions">عملیات</th>
        </tr>
    </thead>
    <tbody>
        @foreach($orders as $item)
        <tr>
            <td>{{ $item->order_number }}</td>
            <td>{{ $item->title }}</td>
            <td style="font-family: sans-serif;font-size:9px">{{ $item->ir_code }}</td>
            <td style="font-family: sans-serif;font-size:9px">{{ $item->ch_code }}</td>
            <td>{{ $item->orderQty }}</td>
            <td>{{ $item->type }}</td>
            <td>{{ $item->desc }}</td>
            <td id="actions">
                <div class="row mx-auto">
                    <form action="{{ url('made-item')}}" method="POST">
                        @csrf
                        @method('patch')
                            <input type="hidden" name="id" value="{{ $item->id }}">
                        <button type="submit" class="btn madeItem" style="background-color: transparent" title="ساخته شد">
                            <i class="fas fa-check text-success"></i>
                        </button>
                    </form>
                    <button class="pt-1 btn" style="background-color: transparent"
                     title="ویرایش" type="button" data-target="#editItemModal-{{ $item->id }}" data-toggle="modal">
                        <i class="fa fa-edit text-info"></i>
                    </button>
                    <div class="modal fade text-right" id="editItemModal-{{ $item->id }}">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                
                                <div class="modal-body" dir="rtl">
                                <form action="{{ url('purchase-items/' . $item->id) }}" method="POST">
                                        @csrf
                                        @method('patch')
                                        <div class="form-group {{ $errors->has('qty') ? 'has-error' : '' }} col-md-6">
                                            <label>تعداد جدید:</label>
                                            <input type="hidden" name="id" value="{{ $item->id }}">
                                            <input type="number" class="form-control" value="{{  $item->orderQty }}" name="qty">
                                            <span class="text-danger">{{ $errors->first('qty') }}</span>
                                        </div>
                                        <button type="submit" class="btn btn-warning">ثبت</button>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">کنسل</button>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                        <form action="{{ url('purchase-items/'.$item->id) }}" method="POST">
                            @csrf
                            @method('delete')
                            <input type="hidden" name="id" value="{{ $item->id }}">
                            <button type="submit" class="btn btn-link delSell" title="حذف">
                                <i class="fa fa-trash text-danger"></i></button>
                        </form>
                        </div>
                        
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

        
    <div class="row mt-2">
        <div class="mx-auto">
            {{$orders->render()}}
        </div>
    </div>
    @else
    <hr>
    <h5 class="text-center text-info">سفارشی موجود نمی باشد</h5>
    @endif
    
</div>
@endsection
@section('scripts')
<script>
    $().ready(function () {
        $('.delSell').click(function () {
            return confirm('آیا از حذف سفارش مورد نظر اطمینان دارید؟');
        });
        $('.madeItem').click(function () {
            return confirm('ساخت شدن قطعه را تایید می کنید؟');
        });
    });
</script>
@endsection
    