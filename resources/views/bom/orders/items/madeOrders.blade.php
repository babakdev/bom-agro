@extends('layouts.adminLayout.admin_design')
@section('content')
<div class="container">
    @include('inc.sessions')
    @if(count($orders)>0)
    <button class="btn btn-danger mt-2" onclick="printContent('madeOrders');">چاپ جدول</button>
    <div id="madeOrders">
<h3 class="text-center">لیست قطعات ساخته شده</h3>
    <table id="example" class="table table-striped table-bordered table-hover text-center" style="overflow: auto"
    dir="rtl">
    <thead>
        <tr class="text-light bg-dark">
            <th class="px-0" style="font-size: 10px">شماره سفارش</th>
            <th>نام قطعه</th>
            <th>کد اگرو</th>
            <th>کد چین</th>
            <th class="px-0" style="font-size: 10px">تعداد</th>
            <th>نوع</th>
            <th>توضیحات</th>
            <th id="actions">عملیات</th>
        </tr>
    </thead>
    <tbody>
        @foreach($orders as $item)
        <tr>
            <td>{{ $item->order_number }}</td>
            <td>{{ $item->title }}</td>
            <td style="font-family: sans-serif;font-size:9px">{{ $item->ir_code }}</td>
            <td style="font-family: sans-serif;font-size:9px">{{ $item->ch_code }}</td>
            <td>{{ $item->orderQty }}</td>
            <td>{{ $item->type }}</td>
            <td>{{ $item->desc }}</td>
            <td id="actions">
                <div class="row mx-auto">
                    <form action="{{ url('made-item')}}" method="POST">
                        @csrf
                        @method('patch')
                            <input type="hidden" name="id" value="{{ $item->id }}">
                        <button type="submit" class="btn madeItem" style="background-color: transparent" title="انتقال به لیست ساخته نشده ها">
                            <i class="fas fa-exclamation text-warning"></i>
                        </button>
                    </form>
                        
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

        
    <div class="row mt-2">
        <div class="mx-auto">
            {{$orders->render()}}
        </div>
    </div>
    @else
    <hr>
    <h5 class="text-center text-info">سفارشی موجود نمی باشد</h5>
    @endif
    
</div>
@endsection
@section('scripts')
<script>
    $().ready(function () {
        $('.madeItem').click(function () {
            return confirm('آیا اطمینان دارید که این قطعه به لیست ساخته نشده ها منتقل شود؟');
        });
    });
</script>
@endsection
    