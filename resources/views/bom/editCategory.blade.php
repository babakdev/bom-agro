@extends('layouts.adminLayout.admin_design')
@section('content')

<div class="container text-right" dir="rtl">
    <img src="{{  asset($item->image) }}" class="img-fluid rounded row mx-auto shadow mb-2" width="300" alt="">

<form action="/edit-category" method="POST" enctype="multipart/form-data">
    @csrf
    @method('patch')

<input type="hidden" value="{{ request('id') }}" name="id">
    <div class="row">
        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }} col-md-6">
            <label>نام:</label>
            <input type="text" class="form-control" value="{{ old('title') ?? $item->title }}" name="title" required>
            <span class="text-danger">{{ $errors->first('title') }}</span>
        </div>

        <div class="form-group {{ $errors->has('qty') ? 'has-error' : '' }} col-md-6">
            <label>تعداد:</label>
            <input type="number" class="form-control" value="{{ old('qty') ?? $item->qty }}" name="qty"  min="0">
            <span class="text-danger">{{ $errors->first('qty') }}</span>
        </div>

    </div>


    <div class="row">
        <div class="form-group {{ $errors->has('ch_code') ? 'has-error' : '' }} col-md-6">
            <label>کد چین:</label>
            <input type="text" class="form-control" value="{{ old('ch_code') ?? $item->ch_code }}" name="ch_code" required>
            <span class="text-danger">{{ $errors->first('ch_code') }}</span>
        </div>

        <div class="form-group {{ $errors->has('ir_code') ? 'has-error' : '' }} col-md-6">
            <label>کد اگرو:</label>
            <input type="text" class="form-control" value="{{ old('ir_code') ?? $item->ir_code }}" name="ir_code" required>
            <span class="text-danger">{{ $errors->first('ir_code') }}</span>
        </div>
    </div>

    <div class="row">

        <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }} col-md-6">
            <label>عکس:</label>
            <input type="file" class="" value="{{ old('image') }}" name="image">
            <span class="text-danger">{{ $errors->first('image') }}</span>
        </div>

        <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }} col-md-6">
            <label>نوع:</label>
            <select type="text" class="form-control" value="{{ old('type') }}" name="type">
                <option {{$item->type == 'تولیدی' ? 'selected' : ''}} value="تولیدی">تولیدی</option>
                <option {{$item->type == 'داخلی' ? 'selected' : ''}} value="داخلی">داخلی</option>
                <option {{$item->type == 'وارداتی' ? 'selected' : ''}} value="وارداتی">وارداتی</option>
            </select>
            <span class="text-danger">{{ $errors->first('type') }}</span>
        </div>
    </div>

    <div class="form-group {{ $errors->has('mat_id') ? 'has-error' : '' }} col-md-12">
        <label>متریال:</label>
        <select type="text" class="form-control" value="{{ old('mat_id') }}" name="mat_id">
            <option value="{{ $item->material ? $item->mat_id : '' }}">{{ $item->material ? $item->material->name : 'نامشخص' }}</option>
            @foreach($mats as $mat)
                <option value="{{ $mat->id }}">{{ $mat->name }}</option>
            @endforeach
        </select>
        <span class="text-danger">{{ $errors->first('type') }}</span>
    </div>

    <div class="row">
        <div class="form-group {{ $errors->has('length') ? 'has-error' : '' }} col-md-6">
            <label>طول:</label>
            <input style="font-family: sans-serif;font-size:small" type="number" class="form-control text-left" dir="ltr" value="{{ old('length') ?? $item->length }}" name="length">
            <span class="text-danger">{{ $errors->first('length') }}</span>
        </div>
        <div class="form-group {{ $errors->has('area') ? 'has-error' : '' }} col-md-6">
            <label>مساحت:</label>
            <input style="font-family: sans-serif;font-size:small" type="number" class="form-control text-left" dir="ltr" value="{{ old('area') ?? $item->area }}" name="area">
            <span class="text-danger">{{ $errors->first('area') }}</span>
        </div>
    </div>

    <div class="form-group {{ $errors->has('desc') ? 'has-error' : '' }} col-md-12">
        <label>توضیحات:</label>
        <input type="text" class="form-control" value="{{ old('desc') ?? $item->desc }}" name="desc">
        <span class="text-danger">{{ $errors->first('desc') }}</span>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-warning btn-lg">ویرایش</button>
    </div>
</form>
@endsection
