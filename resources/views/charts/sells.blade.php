@extends('layouts.adminLayout.admin_design')

@section('content')
<h3 class="page-title text-center py-3 bg-dark text-white">گزارش فروش بهار 1399</h3>
<div class="breadcrumb text-info col-sm-12 p-1 m-0">
    <div class="container-fluid text-right">
        <a href="{{url('/')}}">داشبورد </a><i class="fa fa-chevron-left breadcrumb-item"></i>
        <a href="{{url('/charts')}}">گزارش گیری فروش </a><i class="fa fa-chevron-left breadcrumb-item"></i>
        <a>گزارش تولید شده </a>
    </div>
</div> <!--END BREADCRUMB-->

@include('inc.sessions')

<div class="container-fluid">
    <sell-by-product-graph 
        labels="{{$proNames}}"
        scores="{{$proSells}}">
    </sell-by-product-graph>

    {{--
    <div class='row text-center' dir="rtl">
        @foreach ($data as $item)
        <div class="col-sm-3 card border shadow">
            <div class="card-header">{{ $item->name }}</div>
            <div class="card-body" style="font-size: 16px">
                <b class="badge badge-success font-weight-bold">{{ $item->cnt }}</b> عدد
            </div>
        </div>            
        @endforeach
    </div> 
    --}}
    <hr>
    <input class="form-control w-50 mx-auto mt-2" id="myInput" type="text" placeholder="جستجو در بین محصولات" dir="rtl">
    <br>
    <table class="table text-center table-hover table-striped" dir="rtl">
        <thead class='thead-dark'>
          <tr>
              <th>کد محصول</th>
              <th>نام محصول</th>
            <th>تعداد فروش</th>
          </tr>
        </thead>
        <tbody id="myTable">
            @foreach ($data as $item)
            <?php $name = App\Product::where('code', $item->code)->value('name'); ?>
            @if($item->code != 'S-C')
          <tr>
              <td dir="ltr">{{ $item->code }}</td>
              <td>{{ $name }}</td>
            <td>{{ $item->cnt }}</td>
          </tr>
          @endif
          @endforeach
        </tbody>
      </table>

</div>

@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $("#myInput").on("keyup", function () {
                let value = ($(this).val()).toUpperCase();
                $("#myTable tr").filter(function () {
                    $(this).toggle($(this).text().indexOf(value) > -1)
                });
            });
        });
    </script>
@endsection
