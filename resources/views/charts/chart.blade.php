@extends('layouts.adminLayout.admin_design')

@section('content')
<h3 class="page-title text-center py-3 bg-dark text-white">گزارش گیری</h3>
<div class="breadcrumb text-info col-sm-12 p-1 m-0">
    <div class="container-fluid text-right">
        <a href="{{url('/')}}">داشبورد </a><i class="fa fa-chevron-left breadcrumb-item"></i>
        <a>گزارش گیری فروش </a>
    </div>
</div> <!--END BREADCRUMB-->
@include('inc.sessions')

<div class="container-fluid text-right">
   <form action="/charts/sells" method="GET">
       <div class="form-group">
       <label for="date">بازه زمانی گزارش را انتخاب نمایید</label>
       <select name="date" id="date" class="form-control">
           <option value="1">بهار 99</option>
           <option value="2">تابستان 99</option>
           <option value="3">پاییز 99</option>
           <option value="4">زمستان 99</option>
       </select>
       </div>
       <button type="submit" class="btn btn-dark">گزارش گیری</button>
    
   </form>

</div>

@endsection

