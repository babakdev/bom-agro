@extends('layouts.adminLayout.admin_design')

@section('content')
<h3 class="page-title text-center pt-2">ایمپورت</h3>
@include('inc.sessions')

    <div class="container-fluid">
<form action="/import-order-details" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="file" name="tables[]" value="Upload Excel File" multiple>
    <button type="submit"> ارسال</button>
</form>

    </div>

@endsection
