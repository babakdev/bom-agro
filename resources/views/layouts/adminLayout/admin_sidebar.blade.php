<aside class="left-sidebar" data-sidebarbg="skin5">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="p-t-30">
                <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                    href="/"
                    aria-expanded="false"><i
                    class="fa fa-tachometer-alt"></i><span class="hide-menu">داشبورد </span></a></li>


                    <li class="sidebar-item"><a class="sidebar-link has-arrow waves-effect waves-dark"
                        href="javascript:void(0)" aria-expanded="false"><i
                        class="fab fa-connectdevelop"></i><span class="hide-menu">BOM </span>
                    </a>

                    <ul aria-expanded="false" class="collapse  first-level">

                        <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                            href="{{ url('/category-tree-view') }}"
                            aria-expanded="false"><i class="fas fa-hanukiah"></i><span
                            class="hide-menu"> BOM مدیریت </span></a>
                        </li>
{{--                         <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"--}}
{{--                            href="{{ url('/new-machines-orders') }}"--}}
{{--                            aria-expanded="false"><i class="fas fa-snowplow"></i><span--}}
{{--                            class="hide-menu"> سفارشات جدید ماشین </span></a>--}}
{{--                        </li>--}}
{{--                        <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"--}}
{{--                            href="{{ url('/machines-built') }}"--}}
{{--                            aria-expanded="false"><i class="fas fa-truck-monster"></i><span--}}
{{--                            class="hide-menu"> سفارشات ماشین ساخته شده </span></a>--}}
{{--                        </li>--}}
                        <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                            href="{{ url('/purchase-items') }}"
                            aria-expanded="false"><i class="fas fa-grip-vertical"></i><span
                            class="hide-menu"> سفارشات جدید قطعات </span></a>
                        </li>
                        <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                            href="{{ url('/item-built') }}"
                            aria-expanded="false"><i class="fas fa-grip-horizontal"></i><span
                            class="hide-menu"> سفارشات ساخته شده </span></a>
                        </li>
                    </ul>

                         <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                            href="{{ url('/import-orders') }}"
                            aria-expanded="false"><i class="fa fa-chart-area"></i><span
                            class="hide-menu">ایمپورت فاکتور </span></a>
                        </li>
                        <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                            href="{{ url('/import-order-details') }}"
                            aria-expanded="false"><i class="fa fa-chart-area"></i><span
                            class="hide-menu">ایمپورت ریزفاکتور </span></a>
                        </li>
                        <li class="sidebar-item"><a class="sidebar-link has-arrow waves-effect waves-dark"
                            href="javascript:void(0)" aria-expanded="false"><i
                            class="fa fa-chart-area"></i><span class="hide-menu">نمودارها </span>
                        </a>

                        <ul aria-expanded="false" class="collapse  first-level">

                            <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="{{ url('/charts') }}"
                                aria-expanded="false"><i class="fa fa-images"></i><span
                                class="hide-menu">میزان فروش </span></a>
                            </li>


                        </ul>
                    </li>


                </ul>
            </nav>
        </div>
    </aside>
