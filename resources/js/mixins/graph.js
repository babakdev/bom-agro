import Chart from 'chart.js';

export const graph = {
    template: `
        <div>
               <canvas width="500" height="350" ref="canvas"></canvas>
        </div>
    `,

    methods: {
        createChart(data) {
            Chart.defaults.global.animation.duration = 1000;
            new Chart(this.$refs.canvas, {
                type: data.type,
                data,
                options: {
                    responsive: true,
                    scales: {
                        yAxes: [{
                            ticks: {
                                min: 0,
                                // max:data.max
                            }
                        }]
                    }
                }
            });
        }
    }
};
