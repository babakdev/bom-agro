<?php

use Illuminate\Support\Facades\Route;
use App\Category;
Auth::routes();

Route::get('category-tree-view','CategoryController@manageCategory');
Route::post('add-category','CategoryController@addCategory');
Route::match(['get', 'patch'],'edit-category','CategoryController@editCategory');
Route::get('category/{id}','CategoryController@show');
Route::delete('remove-category','CategoryController@destroy');

// Item Orders
Route::resource('/purchase-items','OrderCategoryController');
Route::patch('/made-item','OrderCategoryController@madeItem');
Route::get('/item-built','OrderCategoryController@itemBuilt');

// Machine Orders
Route::get('/new-machines-orders','MachineOrdersController@newMachineOrders');
Route::get('/machines-built','MachineOrdersController@machinesBuilt');
 Route::post('/new-machine-order','MachineOrdersController@newMachineOrder');


// Route::get('/a', function(){
//     Category::where('parent_id' , 20)->update([
//         'parent_id' => 17
//     ]);
// });


// Route::get('/a', function(){
//     // Product::where('name', 'like' , '%' . 'لوازم یدکی کولتیواتور ' .'%')->update([
//     //     'code' => 'S-C'
//     // ]);
//     OrderDetail::where('code' ,'AMC2-D6EB')->update([
//         'name' => 'کولتیواتور دیزلی 7 اسب استارتی با چرخ 8-400'
//     ]);
// });


Route::get('/', 'MainController@index');
Route::get('/charts', 'MainController@charts');
Route::get('/charts/sells', 'MainController@sellCharts');
Route::match(['get', 'post'], '/import-orders', 'MainController@importOrders');
Route::match(['get', 'post'], '/import-order-details', 'MainController@importOrderDetails');
