<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // DB::table('products')->delete();
         DB::table('products')->insert([
            ['code' => 'AMC2-D11EB', 'name' => 'کولتیواتور دیزلی 12 اسب استارتی با چرخ 12-500', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AMC2-D9EB', 'name' => 'کولتیواتور دیزلی 10 اسب استارتی با چرخ 12-500', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AMC2-D6E', 'name' => 'کولتیواتور دیزلی 7 اسب استارتی با چرخ 8-400', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AMC6-G6', 'name' => 'کولتیواتور بنزینی 6.5 اسب گیربکس تمام دنده', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AMC6-D5', 'name' => 'کولتیواتور دیزلی 5 اسب  با چرخ 8-400', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AMC2-G13', 'name' => 'کولتیواتور بنزینی 13 اسب گیربکس تمام دنده', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AML2-D11EB', 'name' => 'کولتیواتور دیزلی  لوگان 12 اسب استارتی با چرخ 12-500', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AML2-D9EB', 'name' => 'کولتیواتور دیزلی لوگان 10 اسب استارتی با چرخ 12-500', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AML2-D6E', 'name' => 'کولتیواتور دیزلی لوگان 7 اسب استارتی با چرخ 8-400', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AML6-D5', 'name' => 'کولتیواتور دیزلی 5 اسب لوگان باچرخ 8-400', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AML6-G6', 'name' => 'کولتیواتور بنزینی لوگان 6.5 اسب گیربکس تمام دنده', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AG9000E', 'name' => 'موتور برق 7.5kw', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AD6500E', 'name' => 'موتور برق  5kw', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AG3800LT', 'name' => 'موتور برق  3kw', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AG3800-B', 'name' => 'موتور برق  3kw', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AG3000MT', 'name' => 'موتور برق 2.5kw', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AG1500MT', 'name' => 'موتور برق 1.1kw', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AG1500B', 'name' => 'موتور برق 1.1kw', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AG9000ELT', 'name' => 'موتور برق  7.5kw', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AG9000EB', 'name' => 'موتور برق  7.5kw', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AG8000EB', 'name' => 'موتور برق  7.5kw', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AG8000E', 'name' => 'موتور برق  6.5kw', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AG6500E', 'name' => 'موتور برق  5kw', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AG6800E', 'name' => 'موتور برق  5kw', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AG8800E', 'name' => 'موتور برق  7kw', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AMI-T1', 'name' => 'تریلر ثابت', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AMI-T2', 'name' => 'تریلر دمپردار', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'HS-50', 'name' => 'شیلنگ', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'L-S', 'name' => 'لانس', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'HA-45C1', 'name' => 'پمپ سمپاش 45 بار', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'HA-120C1', 'name' => 'پمپ سمپاش 120 بار', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => '110L', 'name' => 'سمپاش فرغونی با مخزن 110 لیتری', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'Z-A', 'name' => 'سمپاش زنبه ای با موتور 6/5 اسب ', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AM-767', 'name' => 'سمپاش موتوری 25 لیتری', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AMI-P1', 'name' => 'گاوآهن تک خیش', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AMI-W6', 'name' => 'چرخ آهنی', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AMI-W3', 'name' => 'چرخ خاک ورز', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => '188FE', 'name' => 'موتور تک دیزلی 12 اسب', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => '186FBE', 'name' => 'موتور تک دیزلی 10 اسب', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => '168', 'name' => 'موتور تک بنزینی 6/5 اسب', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => '5200', 'name' => 'اره زنجیری', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => '5200-45', 'name' => 'اره زنجیری', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'BG435', 'name' => 'علف تراش پشتی ', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'CG435', 'name' => 'علف تراش دوشی', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'CG430', 'name' => 'علف تراش دوشی ', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'BG430', 'name' => 'علف تراش پشتی  ', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'CG520', 'name' => 'علف تراش دوشی ', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'BG520', 'name' => 'علف تراش پشتی  ', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'CG-KNC', 'name' => 'علف تراش دوشی ', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'BG-KNC', 'name' => 'علف تراش پشتی  ', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AD100-30', 'name' => 'پمپ آب دیزلی 4 اینچ', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AD80-30', 'name' => 'پمپ آب دیزلی 3 اینچ', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AG80-30', 'name' => 'پمپ آب بنزینی  3 اینچ', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'AG50-30', 'name' => 'پمپ آب دیزلی 2 اینچ', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'S-C', 'name' => 'لوازم یدکی کولتیواتور', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'S-S', 'name' => 'لوازم یدکی اره', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => 'S-BC', 'name' => 'لوازم یدکی علفتراش', 'unit' => 'دستگاه', 'price' => '0',],
            ['code' => '220L', 'name' => 'سمپاش پشت کولتیواتوری با مخزن 220 لیتری', 'unit' => 'دستگاه', 'price' => '0',],
            
             
         ]);
    }
}
