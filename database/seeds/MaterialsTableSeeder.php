<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MaterialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('materials')->insert([
            ['name' => 'ورق 1mm'],
            ['name' => 'ورق 1.5mm'],
            ['name' => 'ورق 2mm'],
            ['name' => 'ورق 3mm'],
            ['name' => 'ورق 4mm'],
            ['name' => 'ورق 5mm'],
            ['name' => 'ورق 6mm'],
            ['name' => 'ورق 7mm'],
            ['name' => 'ورق 8mm'],
            ['name' => 'ورق 9mm'],
            ['name' => 'ورق 10mm'],
            ['name' => 'ورق 12mm'],
            ['name' => 'لوله محفظه پیچ مالبند'],
            ['name' => 'لوله "2 مالبند'],
        ]);
    }
}
