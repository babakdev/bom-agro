<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mat_id')->nullable();
            $table->string('title');
            $table->unsignedInteger('parent_id')->default(0);
            $table->string('ch_code')->nullable();
            $table->string('ir_code')->nullable();
            $table->string('qty')->default(1);
            $table->enum('type',['تولیدی','داخلی', 'وارداتی'])->default('وارداتی');
            $table->text('desc')->nullable();
            $table->string('image')->nullable();
            $table->bigInteger('length')->nullable();
            $table->bigInteger('area')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
